-- Example plugins file!
-- (suggestion) -> lua/custom/plugins/init.lua or anywhere in custom dir

return {
   { "elkowar/yuck.vim", ft = "yuck" },
   { "ellisonleao/glow.nvim", cmd = "Glow" },
   { "williamboman/nvim-lsp-installer" },
   {
     "nvim-neorg/neorg",
     ft = "norg",
 },
}
